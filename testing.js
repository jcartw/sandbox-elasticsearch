const fetch = require("node-fetch");

const baseUrl = "http://localhost:9200";

// ---------------------------------------------------------------------

// initial GET
// fetch(baseUrl, {
//   method: "GET",
//   headers: { "Content-Type": "application/json" },
// })
//   .then((res) => res.json())
//   .then((json) => console.log(json));

// ---------------------------------------------------------------------

// POST localhost:9200/accounts/person/1
// const body = {
//   name: "John",
//   lastname: "Doe",
//   job_description: "Systems administrator and Linux specialit",
// };

// fetch(`${baseUrl}/accounts/person/1`, {
//   method: "POST",
//   body: JSON.stringify(body),
//   headers: { "Content-Type": "application/json" },
// })
//   .then((res) => res.json())
//   .then((json) => console.log(json));

// {
//   _index: 'accounts',
//   _type: 'person',
//   _id: '1',
//   _version: 1,
//   result: 'created',
//   _shards: { total: 2, successful: 1, failed: 0 },
//   _seq_no: 0,
//   _primary_term: 1
// }

// ---------------------------------------------------------------------

// fetch(`${baseUrl}/accounts/person/1`, {
//   method: "GET",
//   headers: { "Content-Type": "application/json" },
// })
//   .then((res) => res.json())
//   .then((json) => console.log(json));
//
// {
//   _index: 'accounts',
//   _type: 'person',
//   _id: '1',
//   _version: 1,
//   _seq_no: 0,
//   _primary_term: 1,
//   found: true,
//   _source: {
//     name: 'John',
//     lastname: 'Doe',
//     job_description: 'Systems administrator and Linux specialit'
//   }
// }

// ---------------------------------------------------------------------

// const body = {
//   doc: {
//     job_description: "Systems administrator and Linux specialist",
//   },
// };
//
// fetch(`${baseUrl}/accounts/person/1/_update`, {
//   method: "POST",
//   body: JSON.stringify(body),
//   headers: { "Content-Type": "application/json" },
// })
//   .then((res) => res.json())
//   .then((json) => console.log(json));
//
// {
//   _index: 'accounts',
//   _type: 'person',
//   _id: '1',
//   _version: 2,
//   result: 'updated',
//   _shards: { total: 2, successful: 2, failed: 0 },
//   _seq_no: 1,
//   _primary_term: 1
// }

// ---------------------------------------------------------------------

// const body = {
//   name: "John",
//   lastname: "Smith",
//   job_description: "Systems administrator",
// };
//
// fetch(`${baseUrl}/accounts/person/2`, {
//   method: "POST",
//   body: JSON.stringify(body),
//   headers: { "Content-Type": "application/json" },
// })
//   .then((res) => res.json())
//   .then((json) => console.log(json));

// ---------------------------------------------------------------------

// GET localhost:9200/_search?q=john

// fetch(`${baseUrl}/_search?q=john`, {
//   method: "GET",
//   headers: { "Content-Type": "application/json" },
// })
//   .then((res) => res.json())
//   .then((json) => console.log(json));
//
// {
//   took: 528,
//   timed_out: false,
//   _shards: { total: 6, successful: 6, skipped: 0, failed: 0 },
//   hits: {
//     total: { value: 2, relation: 'eq' },
//     max_score: 0.18232156,
//     hits: [ [Object], [Object] ] // search results
//   }
// }

// ---------------------------------------------------------------------

// GET localhost:9200/accounts/person/_search?q=job_description:linux

fetch(`${baseUrl}/accounts/person/_search?q=job_description:linux`, {
  method: "GET",
  headers: { "Content-Type": "application/json" },
})
  .then((res) => res.json())
  .then((json) => {
    console.log(json);
    const result = json.hits.hits.length ? json.hits.hits[0] : "not found";
    console.log(result);
  });
